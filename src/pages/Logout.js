import { Navigate } from 'react-router-dom' 
import UserContext from '../UserContext'
import { useContext, useEffect } from 'react'

export default function Logout(){
	const {unsetUser, setUser} = useContext(UserContext)

	// Using the context, clear the context of the localStorage
	unsetUser()

	// an effect which removes the user.email from the global user state that comes from the context
	useEffect(() => {
		setUser({
			id: null
		})
	}, [])

	return(

	<Navigate to="/login"/>
		)

	

}